
package example.tola.kshrd.topic14_usingasyntask.entity;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PAGINATION {

    @SerializedName("LIMIT")
    private Long mLIMIT;
    @SerializedName("PAGE")
    private Long mPAGE;
    @SerializedName("TOTAL_COUNT")
    private Long mTOTALCOUNT;
    @SerializedName("TOTAL_PAGES")
    private Long mTOTALPAGES;

    public Long getLIMIT() {
        return mLIMIT;
    }

    public Long getPAGE() {
        return mPAGE;
    }

    public Long getTOTALCOUNT() {
        return mTOTALCOUNT;
    }

    public Long getTOTALPAGES() {
        return mTOTALPAGES;
    }

    public static class Builder {

        private Long mLIMIT;
        private Long mPAGE;
        private Long mTOTALCOUNT;
        private Long mTOTALPAGES;

        public PAGINATION.Builder withLIMIT(Long LIMIT) {
            mLIMIT = LIMIT;
            return this;
        }

        public PAGINATION.Builder withPAGE(Long PAGE) {
            mPAGE = PAGE;
            return this;
        }

        public PAGINATION.Builder withTOTALCOUNT(Long TOTALCOUNT) {
            mTOTALCOUNT = TOTALCOUNT;
            return this;
        }

        public PAGINATION.Builder withTOTALPAGES(Long TOTALPAGES) {
            mTOTALPAGES = TOTALPAGES;
            return this;
        }

        public PAGINATION build() {
            PAGINATION PAGINATION = new PAGINATION();
            PAGINATION.mLIMIT = mLIMIT;
            PAGINATION.mPAGE = mPAGE;
            PAGINATION.mTOTALCOUNT = mTOTALCOUNT;
            PAGINATION.mTOTALPAGES = mTOTALPAGES;
            return PAGINATION;
        }

    }

}
