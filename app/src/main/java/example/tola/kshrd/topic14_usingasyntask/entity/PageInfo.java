
package example.tola.kshrd.topic14_usingasyntask.entity;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PageInfo {

    @SerializedName("CODE")
    private String mCODE;
    @SerializedName("DATA")
    private List<User> mDATA;
    @SerializedName("MESSAGE")
    private String mMESSAGE;
    @SerializedName("PAGINATION")
    private example.tola.kshrd.topic14_usingasyntask.entity.PAGINATION mPAGINATION;

    public String getCODE() {
        return mCODE;
    }

    public List<User> getDATA() {
        return mDATA;
    }

    public String getMESSAGE() {
        return mMESSAGE;
    }

    public example.tola.kshrd.topic14_usingasyntask.entity.PAGINATION getPAGINATION() {
        return mPAGINATION;
    }

    public static class Builder {

        private String mCODE;
        private List<User> mDATA;
        private String mMESSAGE;
        private example.tola.kshrd.topic14_usingasyntask.entity.PAGINATION mPAGINATION;

        public PageInfo.Builder withCODE(String CODE) {
            mCODE = CODE;
            return this;
        }

        public PageInfo.Builder withDATA(List<User> DATA) {
            mDATA = DATA;
            return this;
        }

        public PageInfo.Builder withMESSAGE(String MESSAGE) {
            mMESSAGE = MESSAGE;
            return this;
        }

        public PageInfo.Builder withPAGINATION(example.tola.kshrd.topic14_usingasyntask.entity.PAGINATION PAGINATION) {
            mPAGINATION = PAGINATION;
            return this;
        }

        public PageInfo build() {
            PageInfo PageInfo = new PageInfo();
            PageInfo.mCODE = mCODE;
            PageInfo.mDATA = mDATA;
            PageInfo.mMESSAGE = mMESSAGE;
            PageInfo.mPAGINATION = mPAGINATION;
            return PageInfo;
        }

    }

}
