package example.tola.kshrd.topic14_usingasyntask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import example.tola.kshrd.topic14_usingasyntask.adapter.RecyclerViewAdapter;
import example.tola.kshrd.topic14_usingasyntask.entity.User;
import example.tola.kshrd.topic14_usingasyntask.entity.PageInfo;

public class MainActivity extends AppCompatActivity {
    TextView tvTitle;
    Button btGetData;

    RecyclerView recycler;
    RecyclerView.Adapter adapter;
    private List<User> users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        users = new ArrayList<>();

        recycler = (RecyclerView) findViewById(R.id.recycler_view);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        adapter = new RecyclerViewAdapter(users, this);
        recycler.setAdapter(adapter);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        btGetData = (Button) findViewById(R.id.getData);
        btGetData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    new JSONTask().execute(new URL("http://api-ams.me/v1/api/users?page=1&limit=15"));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class JSONTask extends AsyncTask<URL, Void, PageInfo> {

        @Override
        protected PageInfo doInBackground(URL... urls) {

            try {
                //1. connect to url
                URL url = new URL(urls[0].toString());
                URLConnection connection = url.openConnection();
                connection.connect();

                //2. read from stream
                InputStream inputStream = connection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuffer stringBuffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    stringBuffer.append(line);
                }

                //3. convert from JSON to Java Object
                Gson gson = new Gson();
                Type type = new TypeToken<PageInfo>() {
                }.getType();
                String json = stringBuffer.toString();
                PageInfo fromJson = gson.fromJson(json, type);

                return fromJson;

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(PageInfo pageInfo) {
            super.onPostExecute(pageInfo);

            //4. get data and notify DataSet changed
            users.addAll(pageInfo.getDATA());
            adapter.notifyDataSetChanged();
        }
    }
}
