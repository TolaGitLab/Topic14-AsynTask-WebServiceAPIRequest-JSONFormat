
package example.tola.kshrd.topic14_usingasyntask.entity;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class User {

    @SerializedName("ID")
    private Long mID;
    @SerializedName("NAME")
    private String mNAME;
    @SerializedName("GENDER")
    private String mGENDER;
    @SerializedName("EMAIL")
    private String mEMAIL;
    @SerializedName("FACEBOOK_ID")
    private String mFACEBOOKID;
    @SerializedName("IMAGE_URL")
    private String mIMAGEURL;
    @SerializedName("STATUS")
    private String mSTATUS;
    @SerializedName("TELEPHONE")
    private String mTELEPHONE;

    public String getEMAIL() {
        return mEMAIL;
    }

    public String getFACEBOOKID() {
        return mFACEBOOKID;
    }

    public String getGENDER() {
        return mGENDER;
    }

    public Long getID() {
        return mID;
    }

    public String getIMAGEURL() {
        return mIMAGEURL;
    }

    public String getNAME() {
        return mNAME;
    }

    public String getSTATUS() {
        return mSTATUS;
    }

    public String getTELEPHONE() {
        return mTELEPHONE;
    }

    public static class Builder {

        private String mEMAIL;
        private String mFACEBOOKID;
        private String mGENDER;
        private Long mID;
        private String mIMAGEURL;
        private String mNAME;
        private String mSTATUS;
        private String mTELEPHONE;

        public User.Builder withEMAIL(String EMAIL) {
            mEMAIL = EMAIL;
            return this;
        }

        public User.Builder withFACEBOOKID(String FACEBOOKID) {
            mFACEBOOKID = FACEBOOKID;
            return this;
        }

        public User.Builder withGENDER(String GENDER) {
            mGENDER = GENDER;
            return this;
        }

        public User.Builder withID(Long ID) {
            mID = ID;
            return this;
        }

        public User.Builder withIMAGEURL(String IMAGEURL) {
            mIMAGEURL = IMAGEURL;
            return this;
        }

        public User.Builder withNAME(String NAME) {
            mNAME = NAME;
            return this;
        }

        public User.Builder withSTATUS(String STATUS) {
            mSTATUS = STATUS;
            return this;
        }

        public User.Builder withTELEPHONE(String TELEPHONE) {
            mTELEPHONE = TELEPHONE;
            return this;
        }

        public User build() {
            User User = new User();
            User.mEMAIL = mEMAIL;
            User.mFACEBOOKID = mFACEBOOKID;
            User.mGENDER = mGENDER;
            User.mID = mID;
            User.mIMAGEURL = mIMAGEURL;
            User.mNAME = mNAME;
            User.mSTATUS = mSTATUS;
            User.mTELEPHONE = mTELEPHONE;
            return User;
        }

    }

}
